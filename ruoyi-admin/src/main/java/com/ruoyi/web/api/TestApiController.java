package com.ruoyi.web.api;

import com.ruoyi.system.domain.SysDictType;
import com.ruoyi.system.service.ISysDictTypeService;
import com.ruoyi.web.api.common.Rest;
import io.swagger.annotations.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(value = "测试",tags = "测试接口")
@RestController
@RequestMapping("/api/test")
public class TestApiController {

    @Autowired
    private ISysDictTypeService dictTypeService;

    @ApiResponses({@ApiResponse(code=200,message="成功"),
            @ApiResponse(code=100,message="服务器异常")
    })
    @ApiOperation(value = "获取所有字典类型")
    @GetMapping("test")
    public Rest<SysDictType> test(TestQueryDto query){
        Rest rest = new Rest();
        try {
            List<SysDictType> sysDictTypes = dictTypeService.selectDictTypeAll();
            rest.setData(sysDictTypes);
            rest.setErrCode(200);
            rest.setMessage("成功");
        } catch (Exception e) {
            e.printStackTrace();
            rest.setErrCode(100);
            rest.setMessage("服务器异常");
        }
        return rest;
    }

    @Data
    static class TestQueryDto{
        @ApiModelProperty(value = "字典类型",example = "user_type")
        private String type;
        @ApiModelProperty(value = "字典类型code",example = "1",required = true)
        private Integer code;
    }

}
