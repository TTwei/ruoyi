package com.ruoyi.web.api.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Rest<T> {

    @ApiModelProperty(value = "是否成功")
    private boolean success=true;
    @ApiModelProperty(value = "返回对象")
    private T data;
    @ApiModelProperty(value = "错误编号",example = "200")
    private Integer errCode;
    @ApiModelProperty(value = "错误信息",example = "成功")
    private String message;

}
