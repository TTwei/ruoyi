package com.ruoyi.web.controller.module;

import java.util.List;
import java.util.Arrays;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.gold.domain.TGoldPrice;
import com.ruoyi.gold.service.ITGoldPriceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金币价格Controller
 * 
 * @author ruoyi
 * @date 2019-11-25
 */
@Controller
@RequestMapping("/gold/price")
public class TGoldPriceController extends BaseController
{
    private String prefix = "gold/price";

    @Autowired
    private ITGoldPriceService tGoldPriceService;

    @RequiresPermissions("gold:price:view")
    @GetMapping()
    public String price()
    {
        return "gold/price/price";
    }

    /**
     * 查询金币价格列表
     */
    @RequiresPermissions("gold:price:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TGoldPrice tGoldPrice)
    {
        startPage();
        return getDataTable(tGoldPriceService.list(new QueryWrapper<>()));
    }

    /**
     * 导出金币价格列表
     */
    @RequiresPermissions("gold:price:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TGoldPrice tGoldPrice)
    {
        List<TGoldPrice> list = tGoldPriceService.list(new QueryWrapper<>());
        ExcelUtil<TGoldPrice> util = new ExcelUtil<TGoldPrice>(TGoldPrice.class);
        return util.exportExcel(list, "price");
    }

    /**
     * 新增金币价格
     */
    @GetMapping("/add")
    public String add()
    {
        return "gold/price/add";
    }

    /**
     * 新增保存金币价格
     */
    @RequiresPermissions("gold:price:add")
    @Log(title = "金币价格", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TGoldPrice tGoldPrice)
    {
        boolean save = tGoldPriceService.save(tGoldPrice);
        System.out.println(tGoldPrice.getId());
        return  toAjax(save);
    }

    /**
     * 修改金币价格
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        TGoldPrice tGoldPrice = tGoldPriceService.getById(id);
        mmap.put("tGoldPrice", tGoldPrice);
        return "gold/price/edit";
    }

    /**
     * 修改保存金币价格
     */
    @RequiresPermissions("gold:price:edit")
    @Log(title = "金币价格", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TGoldPrice tGoldPrice)
    {
        return toAjax(tGoldPriceService.updateById(tGoldPrice));
    }

    /**
     * 删除金币价格
     */
    @RequiresPermissions("gold:price:remove")
    @Log(title = "金币价格", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tGoldPriceService.removeByIds(Arrays.asList(Convert.toStrArray(ids))));
    }
}
