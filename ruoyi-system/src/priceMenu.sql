-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('金币价格', '3', '1', '/gold/price', 'C', '0', 'gold:price:view', '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '金币价格菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('金币价格查询', @parentId, '1',  '#',  'F', '0', 'gold:price:list',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('金币价格新增', @parentId, '2',  '#',  'F', '0', 'gold:price:add',          '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('金币价格修改', @parentId, '3',  '#',  'F', '0', 'gold:price:edit',         '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');

insert into sys_menu  (menu_name, parent_id, order_num, url,menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('金币价格删除', @parentId, '4',  '#',  'F', '0', 'gold:price:remove',       '#', 'admin', '2018-03-01', 'ry', '2018-03-01', '');
