package com.ruoyi.gold.mapper;

import com.ruoyi.gold.domain.TGoldPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 金币价格 数据层
 *
 * @author ruoyi
 * @date 2019-11-25
 */
public interface TGoldPriceMapper extends BaseMapper<TGoldPrice>
{

}