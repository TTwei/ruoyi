package com.ruoyi.gold.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.io.Serializable;
import java.util.Date;
import com.ruoyi.common.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
                                    
/**
 * 金币价格表 t_gold_price
 *
 * @author ruoyi
 * @date 2019-11-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_gold_price")
public class TGoldPrice  extends BaseEntity implements Serializable
        {
private static final long serialVersionUID = 1L;

/** id */
@TableId(type = IdType.AUTO)
private Long id;
/** 苹果对应的id */
private String appleId;
/** 金币数量 */
private Long goldNumber;
/** 金币价格 */
private Long goldPrice;
}