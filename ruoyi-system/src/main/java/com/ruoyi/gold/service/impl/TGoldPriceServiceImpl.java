package com.ruoyi.gold.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.ruoyi.gold.mapper.TGoldPriceMapper;
import com.ruoyi.gold.domain.TGoldPrice;
import com.ruoyi.gold.service.ITGoldPriceService;

/**
 * 金币价格 服务层实现
 *
 * @author ruoyi
 * @date 2019-11-25
 */
@Service
public class TGoldPriceServiceImpl extends ServiceImpl<TGoldPriceMapper, TGoldPrice> implements ITGoldPriceService
{

}