package com.ruoyi.gold.service;

import com.ruoyi.gold.domain.TGoldPrice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 金币价格 服务层
 *
 * @author ruoyi
 * @date 2019-11-25
 */
public interface ITGoldPriceService extends IService<TGoldPrice>
{

}